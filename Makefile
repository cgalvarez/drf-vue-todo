init:
	@# Initializes the project.
	@#   Usage: `make init`
	@python3 -m venv .venv && \
	. .venv/bin/activate && \
	pip install --upgrade pip && \
	pip install -r requirements-dev.txt && \
	pip install -r requirements-prod.txt && \
	python manage.py makemigrations && \
	python manage.py migrate && \
	(cd frontend_vue && npm i)

django-clear:
	@# Remove all .pyc files and __pycache__ folders.
	@# Usage: `make django-clear`
	find drf_vue_todo -type d -name __pycache__ -exec rm -rf {} \;
	find drf_vue_todo -type f -name "*.pyc" -exec rm -f {} \;
	find django_apps -type d -name __pycache__ -exec rm -rf {} \;
	find django_apps -type f -name "*.pyc" -exec rm -f {} \;

django-create-app:
	@# Create a Django app.
	@#   Usage: `make django-create-app APP=myapp`
	@. .venv/bin/activate && \
	python manage.py startapp $(APP)

django-devserver:
	@# Run the Django development web server over HTTP and
	@# open the project in the user's preferred browser.
	@#   Usage: `make django-devserver`
	@# NOTE: Defaults to localhost:8000
	@((sleep 2 && python -m webbrowser http://localhost:8000) &) ; \
	. .venv/bin/activate && \
	printf 'Using ' && python --version && \
	python manage.py runserver

django-test:
	@# Run the tests for Django backend.
	@#   Usage: `make django-test`
	@. .venv/bin/activate && \
	python manage.py test backend_django

django-migratedb:
	@# Make migration updates and apply.
	@#   Usage: `make django-migratedb`
	@. .venv/bin/activate && \
	python manage.py makemigrations && \
	python manage.py migrate

django-resetdb:
	@# Drops the current SQLite database.
	@#   Usage: `make django-resetdb`
	@. .venv/bin/activate && \
	python manage.py flush

pytest:
	@# Run all tests configured with PyTest.
	@#   Usage: `make pytest`
	@. .venv/bin/activate && \
	pytest

vue-devserver:
	@# Run the VueJS development web server over HTTP and
	@# open the project in the user's preferred browser.
	@#   Usage: `make vue-devserver`
	@# NOTE 1: Defaults to localhost:8080
	@# NOTE 2: Opening localhost:8080 won't work,
	@#         since it's now bridged through Django
	@cd frontend_vue ; npm run serve

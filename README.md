# drf-vue-todo

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](CODE_OF_CONDUCT.md)
 [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)

**A TODO web app made with Django + DRF + VueJS + Vuetify + Axios.**

This app can be used to check how to integrate a Django backend with a VueJS/Vuetify frontend.

The VueJS frontend is splitted into multiple chunks, so you can check how to handle that case to suit your needs.

The code also shows how to receive the Django context in the target template without messing with setting alternative delimiters as I've seen on other tutorials.

Also covers how to transform data properly to pass it in the Django context and how to securely read it.

**NOTE**: This app is a WIP (Work In Progress), so please, be gentle and friendly! :D

## Prerrequisites

You need Node.js and Python installed.

## Python + Django + DRF (Django Rest Framework)

This app uses Django 2.2, so you will need Python 3.5+.

The best you can do to avoid messing with your OS python version is creating a virtual environment. The project has a Makefile recipe ready for you to create the virtual environment and install the required packages for both, development and production environments:

```shell
drf-vue-todo$ make init
```

**NOTE**: If the execution of the Makefile recipe throws any error of missing packages, just install them.

## Node.js

The frontend of this app uses VueJS + Vuetify, both relying on the VueCLI, which runs over Node.js.

```bash
# Installing of Node.js is a piece of cake with NVM.
$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
# Ensure it is properly installed.
$ command -v nvm && nvm --version
# Install latest version of Node.js and use it.
$ nvm install node
$ nvm use node
# Ensure it is properly installed.
$ node --version  # Should output someting like "v12.3.1"
```

## Quickstart

```shell
$ git clone https://gitlab.com/cgalvarez/drf-vue-todo.git
$ cd drf-vue-todo
drf-vue-todo$ make init  # Exec this only first time to set the project up.
```

Running the app locally requires runnnig two separate servers:

- One server for Django, which acts as the backend.
- Another server for Vuetify, which acts as the frontend.

**NOTE**: Although it is possible running one of the servers in the background, I highly discourage it, because reading the output of each server can be really helpful to catch and fix any error. So...

On first terminal (this takes longer):

```shell
drf-vue-todo$ make vue-devserver
```

On second terminal (last, since Django depends on the VueJS dev server to run properly):

```shell
drf-vue-todo$ make django-devserver
```

## Code of conduct

Please note that this project is released with a Contributor Code of Conduct. By participating in this project you agree to abide by its terms.

## Development / contributing

PR welcome!!

This project is pre-configured to be developed with [VSCodium](https://vscodium.com/), which is the FLOSS (Free/Libre Open Source Software) version of [VSCode](https://code.visualstudio.com/). Both should work fine. I just prefer VSCodium because it has *Telemetry* disabled and is compliant with the FLOSS philosophy.

## Roadmap

- [ ] Login/logout feature.
  - [ ] DO NOT render sidebar if user not logged in.
- [ ] i18n:
  - [ ] Handle gender properly with helper.
- [ ] `goals` app
  - [x] Create goals.
  - [x] Remove goals.
  - [ ] Edit goals.

from django.apps import AppConfig


class GoalsConfig(AppConfig):
    name = 'django_apps.goals'
    verbose_name = 'Goals'

from django.db import models


class Goal(models.Model):
    """A task to be done."""

    title = models.CharField(max_length=25)
    definition = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    deadline = models.DateField()
    owner = models.ForeignKey('auth.user', blank=False, on_delete=models.PROTECT)

    def __str__(self):
        """Return representation of model."""
        return self.title

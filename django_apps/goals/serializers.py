from rest_framework import serializers

from .models import Goal


class GoalSerializer(serializers.ModelSerializer):
    """Define the API representation."""

    owner = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        return Goal.objects.create(owner=self.context['request'].user,
                                   **validated_data)

    class Meta:
        model = Goal
        fields = ('id', 'title', 'definition', 'deadline', 'owner')

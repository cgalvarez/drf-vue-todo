from rest_framework import viewsets
from rest_framework.response import Response

from .models import Goal
from .serializers import GoalSerializer


class GoalViewSet(viewsets.ModelViewSet):
    """Define the view behavior."""

    queryset = Goal.objects.order_by('-deadline')
    serializer_class = GoalSerializer

    def list(self, request):
        queryset = Goal.objects.filter(owner=request.user).order_by('-deadline')
        serializer = GoalSerializer(queryset, many=True)
        return Response(serializer.data)

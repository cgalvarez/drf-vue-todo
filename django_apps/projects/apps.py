from django.apps import AppConfig


class ProjectsConfig(AppConfig):
    name = 'django_apps.projects'
    verbose_name = 'Projects'

# Generated by Django 2.2.2 on 2019-06-23 14:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('goals', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=25)),
                ('description', models.CharField(default='', max_length=255)),
                ('iconName', models.CharField(default='', max_length=30)),
                ('iconColor', models.CharField(default='', max_length=20)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('deadline', models.DateField()),
                ('goals', models.ManyToManyField(to='goals.Goal')),
            ],
        ),
    ]

from django.db import models

from django_apps.goals.models import Goal


class Project(models.Model):
    """A project which contributes to at least one of the user's goals."""

    title = models.CharField(max_length=25)
    description = models.CharField(max_length=255, default='')
    iconName = models.CharField(max_length=30, default='')
    iconColor = models.CharField(max_length=20, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey('auth.user', blank=False, related_name='project_owner', on_delete=models.PROTECT)
    members = models.ManyToManyField('auth.user', blank=False, related_name='project_member')
    deadline = models.DateField()
    goals = models.ManyToManyField('goals.Goal')

    def __str__(self):
        """Return representation of model."""
        pattern = 'Project(id={id}, owner={owner}, title="{title}", ' \
                  + 'description="{description}", deadline={deadline}, '
        return pattern.format(
            id=self.id,
            title=self.title,
            description=self.description,
            deadline=self.deadline.strftime('%Y-%m-%d'),
            owner=self.owner,
            members=self.members,
            goals=self.goals,
        )

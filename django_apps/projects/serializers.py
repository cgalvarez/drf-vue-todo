from rest_framework import serializers

# from django_apps.goals.serializers import GoalSerializer
from django_apps.goals.models import Goal
from .models import Project


class WriteProjectSerializer(serializers.ModelSerializer):
    """Define the API representation."""
    owner = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        user = self.context['request'].user

        # Pop m2m fields.
        members = validated_data.pop('members', None)
        goals = validated_data.pop('goals')

        # Project instance.
        project = Project.objects.create(owner=user, **validated_data)

        # ManyToMany fields.
        project.members.add(*members if members else user)
        project.goals.add(*goals)

        return project


    class Meta:
        model = Project
        fields = ('id', 'title', 'description', 'deadline', 'goals', 'owner', 'members')


class GoalSerializer(serializers.ModelSerializer):
    """Serialize a goal to fit into projects list."""
    class Meta:
        model = Goal
        fields = ('id', 'title')


class ReadProjectSerializer(serializers.ModelSerializer):
    """Define the API representation."""
    goals = GoalSerializer(many=True)
    owner_name = serializers.SerializerMethodField('owner_fullname')
    #goals = serializers.SlugRelatedField(
    #    read_only=True, many=True, allow_null=True, slug_field='title')

    def owner_fullname(self, project):
        return '{} {}'.format(project.owner.first_name, project.owner.last_name)

    class Meta:
        model = Project
        fields = ('id', 'title', 'description', 'deadline', 'goals', 'owner_name', 'members')

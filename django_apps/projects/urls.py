from django.conf.urls import include, url
from rest_framework import routers

from .views import ProjectViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'projects', ProjectViewSet)

urlpatterns = [
    url("^", include(router.urls)),
]

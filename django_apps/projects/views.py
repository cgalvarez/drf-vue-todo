from copy import deepcopy

from knox.auth import TokenAuthentication
from rest_framework import authentication, permissions, status, viewsets
from rest_framework.response import Response

from .models import Project
from .serializers import ReadProjectSerializer, WriteProjectSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    """Define the view behavior."""

    queryset = Project.objects.all()  # .order_by('-deadline')
    #authentication_classes = (TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get_serializer_class(self):
        return WriteProjectSerializer \
            if self.action in ['create', 'partial_update', 'update'] \
            else ReadProjectSerializer

    def _write(self, request, *args, **kwargs):
        data = request.data
        is_update = 'pk' in kwargs
        if is_update:
            saved = Project.objects.get(id=kwargs['pk'])
            if not data.get('members', None):
                data['members'] = [member.id for member in saved.members.all()]
            if not data.get('goals'):
                data['goals'] = [goal.id for goal in saved.goals.all()]
            serialized = WriteProjectSerializer(saved, data=data)
        else:
            data['owner'] = request.user.id
            data['members'] = [request.user.id]
            serialized = WriteProjectSerializer(data=data, context={'request': request})
        print('serialized:', serialized)

        if serialized.is_valid():
            obj = serialized.save()
            return Response(
                ReadProjectSerializer(obj).data,
                status=status.HTTP_200_OK if is_update else status.HTTP_201_CREATED)
        return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        return self._write(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return self._write(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        return self._write(request, *args, **kwargs)

    def list(self, request):
        queryset = Project.objects.filter(owner=request.user)  # .order_by('-deadline')
        serializer = ReadProjectSerializer(queryset, many=True)
        return Response(serializer.data)

from django.apps import AppConfig


class TasksConfig(AppConfig):
    name = 'django_apps.tasks'
    verbose_name = 'Tasks'

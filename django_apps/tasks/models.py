from django.db import models


class Task(models.Model):
    """A task to be done."""

    title = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    done = models.BooleanField(default=False)

    def __str__(self):
        """Return representation of model."""
        return self.title

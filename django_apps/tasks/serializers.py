from rest_framework import serializers

from .models import Task


class TaskSerializer(serializers.ModelSerializer):
    """Define the API representation."""

    class Meta:
        model = Task
        fields = ('id', 'title', 'done', )

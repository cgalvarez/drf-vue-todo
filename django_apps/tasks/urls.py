from django.conf.urls import include, url
from rest_framework import routers

from .views import TaskViewSet


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)

urlpatterns = [
    url("^", include(router.urls)),
]

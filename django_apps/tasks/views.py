from rest_framework import viewsets, permissions

from .models import Task
from .serializers import TaskSerializer


class TaskViewSet(viewsets.ModelViewSet):
    """Define the view behavior."""

    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    # permission_classes = [permissions.AllowAny, ]

"""drf_vue_todo URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/

Examples
--------
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from knox import views as knox_views

from django_apps.goals import urls as goalsEndpoints
from django_apps.projects import urls as projectsEndpoints
from django_apps.tasks import urls as tasksEndpoints

from .views import IndexTemplateView, LoginView


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/auth/signin/', LoginView.as_view(), name='knox_login'),
    url(r'^api/auth/signout/', knox_views.LogoutView.as_view(), name='knox_logout'),
    url(r'^api/auth/signoutall/', knox_views.LogoutAllView.as_view(), name='knox_logoutall'),
    url(r'^api/auth/registration/', include('rest_auth.registration.urls')),
    url(r'^api/', include(goalsEndpoints)),
    url(r'^api/', include(projectsEndpoints)),
    url(r'^api/', include(tasksEndpoints)),
    path('', IndexTemplateView.as_view(), name='index', ),
]

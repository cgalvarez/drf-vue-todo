import {apiHref} from '../constants';


const goalsApi = new URL('goals/', apiHref).href;

const api = {
  get(id) { return id ? new URL(id + '/', goalsApi) : goalsApi; },
  post(id) { return id ? new URL(id + '/', goalsApi) : goalsApi; },
};


export {
  api,
};

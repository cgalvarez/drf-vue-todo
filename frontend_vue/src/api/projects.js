import {apiHref} from '../constants';

const projectsApi = new URL('projects/', apiHref).href;

const api = {
  get(id) { return id ? new URL(id + '/', projectsApi) : projectsApi; },
  post(id) { return id ? new URL(id + '/', projectsApi) : projectsApi; },
};

export {
  api,
};

import {apiHref} from '../constants';

const tasksApi = new URL('tasks/', apiHref).href;

const api = {
  get(id) { return id ? new URL(id, tasksApi) : tasksApi; },
  post(id) { return id ? new URL(id, tasksApi) : tasksApi; },
};

export {
  api,
};

import '@mdi/font/css/materialdesignicons.min.css';
import 'animate.css/animate.min.css';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import {i18n} from '../plugins/vuetify';
import Entrypoint from './Entrypoint.vue';

// Routes.
import Landing from '../components/landing';
import Goals from '../components/goals/Goals';
import Projects from '../components/projects/Projects';
import Tasks from '../components/tasks/Tasks';

// Store modules.
import account from '../store/account';

// Configure `vue-router`.
Vue.use(VueRouter);
Vue.config.productionTip = false;
const routes = [
  { path: '/', component: Landing },
  { path: '/goals', component: Goals },
  { path: '/projects', component: Projects },
  { path: '/tasks', component: Tasks },
];
const router = new VueRouter({ routes });

// Configure `vuex`.
Vue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    account,
  },
});

new Vue({
  i18n,
  router,
  store,
  render: h => h(Entrypoint),
}).$mount('#app');

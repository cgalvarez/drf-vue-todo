export default {
  goals: {
    btn: {
      cancel: 'Cancel',
      newGoal: 'New goal',
      save: 'Save',
      action: {
        edit: 'Edit this goal',
        remove: 'Remove this goal',
      },
    },
    formTitle: {
      new: 'Create new goal',
      edit: 'Edit goal',
    },
    label: {
      deadline: 'Deadline',
      goals: 'Goals',
      title: 'Write your goal title',
      definition: 'Write your goal definition',
      definitionPrefix: 'I want to',
    },
    table: {
      header: {
        title: 'Goal',
        deadline: 'Deadline',
        actions: 'Actions',
      },
      noData: 'Sorry, there is nothing to display',
    },
  },
};

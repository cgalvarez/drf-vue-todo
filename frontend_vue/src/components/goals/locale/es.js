export default {
  goals: {
    btn: {
      cancel: 'Cancelar',
      newGoal: 'Nueva meta',
      save: 'Guardar',
      action: {
        edit: 'Editar esta meta',
        remove: 'Eliminar esta meta',
      },
    },
    formTitle: {
      new: 'Crear nueva meta',
      edit: 'Editar meta',
    },
    label: {
      deadline: 'Fecha límite',
      goals: 'Metas',
      title: 'Escribe el nombre de tu meta',
      definition: 'Escribe la definición de tu meta',
      definitionPrefix: 'Quiero',
    },
    table: {
      header: {
        title: 'Meta',
        deadline: 'Fecha límite',
        actions: 'Acciones',
      },
      noData: 'Lo sentimos, no hay nada que mostrar',
    },
  },
};

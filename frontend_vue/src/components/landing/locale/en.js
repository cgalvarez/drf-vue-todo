export default {
  landing: {
    callsToAction: {
      purpose: 'Ignite your passion and purpose',
      goals: 'Settle your goals',
      projects: 'Work in projects that contribute you',
      tasks: 'Organize to get things done',
      scoreboard: 'Closely monitor everything',
    },
  },
};

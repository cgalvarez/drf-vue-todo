export default {
  landing: {
    callsToAction: {
      purpose: 'Desata tu pasión y tu propósito',
      goals: 'Establece tus metas',
      projects: 'Trabaja en proyectos que te aporten',
      tasks: 'Organízate para terminar tus tareas',
      scoreboard: 'Controla atentamente todo',
    },
  },
};

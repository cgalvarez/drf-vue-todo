export default {
  projects: {
    btn: {
      cancel: 'Cancel',
      new: 'New project',
      save: 'Save',
      action: {
        edit: 'Edit this project',
        remove: 'Remove this project',
      },
    },
    formTitle: {
      new: 'Create new project',
      edit: 'Edit project',
    },
    label: {
      deadline: 'Deadline',
      description: 'Write your project description',
      goals: 'Select the goals which this project contributes to...',
      items: 'Projects',
      title: 'Write your project title',
    },
    table: {
      header: {
        actions: 'Actions',
        deadline: 'Deadline',
        goals: 'Goals',
        summary: 'Project',
      },
      noData: 'Sorry! There is nothing to display.',
    },
  },
};

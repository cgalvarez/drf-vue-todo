export default {
  projects: {
    btn: {
      cancel: 'Cancelar',
      new: 'Nuevo proyecto',
      save: 'Guardar',
      action: {
        edit: 'Editar este proyecto',
        remove: 'Eliminar este proyecto',
      },
    },
    formTitle: {
      new: 'Crear nuevo proyecto',
      edit: 'Editar proyecto',
    },
    label: {
      deadline: 'Fecha límite',
      description: 'Escribe la descripción de tu proyecto',
      goals: 'Selecciona las metas a las que este proyecto contribuye...',
      items: 'Proyectos',
      title: 'Escribe el título de tu proyecto',
    },
    table: {
      header: {
        actions: 'Acciones',
        deadline: 'Fecha límite',
        goals: 'Metas',
        summary: 'Proyecto',
      },
      noData: '¡Se siente! No hay nada que mostrar.',
    },
  },
};

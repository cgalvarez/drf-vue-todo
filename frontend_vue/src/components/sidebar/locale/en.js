export default {
  sidebar: {
    label: {
      title: 'Sections',
    },
    section: {
      goals: 'Goals',
      projects: 'Projects',
      tasks: 'Tasks',
    },
  },
};

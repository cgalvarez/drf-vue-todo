export default {
  sidebar: {
    label: {
      title: 'Secciones',
    },
    section: {
      goals: 'Metas',
      projects: 'Proyectos',
      tasks: 'Tareas',
    },
  },
};

export default {
  sign: {
    error: {
      minLength: 'Must have at least 8 characters.',
      passphrasesDontMatch: "The passphrases you entered don't match.",
      required: 'Required.',
    },
    label: {
      sign: 'Sign',
      username: 'Username',
      passphrase: 'Passphrase',
      passphraseVerification: 'Verify your passphrase',
    },
    tabs: {
      up: 'Sign up',
      in: 'Sign in',
      out: 'Sign out',
    },
  },
};

export default {
  sign: {
    error: {
      minLength: 'Debe tener al menos 8 caracteres.',
      passphrasesDontMatch: 'Las frases de seguridad no coinciden.',
      required: 'Obligatorio.',
    },
    label: {
      sign: 'Acceder',
      username: 'Nombre de usuario',
      passphrase: 'Frase de seguridad',
      passphraseVerification: 'Verifica tu frase de seguridad',
    },
    tabs: {
      up: 'Registrarse',
      in: 'Acceder',
      out: 'Salir',
    },
  },
};

export default {
  tasks: {
    status: {
      completed: 'Completed:',
      remaining: 'Remaining:',
    },
    writeTask: 'Write a new task...',
    todaySprint: 'Today sprint: no tasks | Today sprint: 1 task | Today sprint: {count} tasks',
  },
};

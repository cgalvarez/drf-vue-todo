export default {
  tasks: {
    status: {
      completed: 'Completadas:',
      remaining: 'Restantes:',
    },
    writeTask: 'Escribe una nueva tarea...',
    todaySprint: 'Sprint de hoy: ninguna tarea | Sprint de hoy: 1 tarea | Sprint de hoy: {count} tareas',
  },
};

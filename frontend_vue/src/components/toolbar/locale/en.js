export default {
  toolbar: {
    btn: {
      language: {
        label: 'Language',
        tip: 'Change the language',
      },
    },
    language: {
      es: 'Español',
      en: 'English',
    },
  },
};

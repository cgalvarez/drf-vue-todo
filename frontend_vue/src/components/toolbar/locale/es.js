export default {
  toolbar: {
    btn: {
      language: {
        label: 'Idioma',
        tip: 'Cambia el idioma',
      },
    },
    language: {
      es: 'Español',
      en: 'English',
    },
  },
};

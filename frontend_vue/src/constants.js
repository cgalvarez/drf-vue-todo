// See https://nodejs.org/api/url.html#url_url_strings_and_url_objects
const urlOrigin = 'http://localhost:8000/';
const apiHref = new URL('api/', urlOrigin).href;

export {
  apiHref,
  urlOrigin,
};

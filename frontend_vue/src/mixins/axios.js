import axios from 'axios';

const Axios = {
  beforeCreate() {
    this.$axios = axios;
  },

  methods: {
    axiosRequestErrorHandler (error) {
      /* eslint-disable no-console */
      if (error.response) {
        const {response} = error;
        console.groupCollapsed(`Axios "${response.config.method}" request failed `
          + `with status code ${response.status} (${response.statusText}) `
          + `to ${response.config.url}`);
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx.
        console.log('Response data:', error.response.data);
        console.log('Response headers:', error.response.headers);
        console.log('Axios config:', error.config);
        console.groupEnd();
      } else if (error.request) {
        // The request was made but no response was received `error.request`
        // is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in Node.js.
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error.
        console.log('Axios error when handling response:', error);
      }
      /* eslint-enable no-console */
    },
  },
};

export default Axios;

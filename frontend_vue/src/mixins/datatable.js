const DataTable = {
  methods: {
    /**
     * The sorting feature for a Vuetify data-table is provided by Vuetify
     * mixin `data-iterable`.
     *
     * Vuetify switches the sorting order on a column on each click on the
     * header cell by calling to `.sort()`, which internally calls to
     * `.updatePagination({ sortBy: header.value, descending: true/false })`.
     *
     * @param {VueComponent} vComponent  The data-table component instance.
     * @see https://github.com/vuetifyjs/vuetify/blob/master/packages/vuetify/src/mixins/data-iterable.js#L306
     */
    deferredReorder(vComponent) {
      const th = vComponent.$el.querySelectorAll('th.active')[0];
      const direction = th.getAttribute('aria-sort') || 'ascending';
      const column = this.headers.find(h => (h.text === th.innerText)).value;
      const params = {sortBy: column, descending: direction === 'descending'};
      this.$nextTick(() => vComponent.updatePagination(params));
    },
  },
};

export default DataTable;

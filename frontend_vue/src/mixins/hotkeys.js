import keypress from 'keypress.js';


const listener = new keypress.Listener();

const Hotkeys = {
  created() {
    this.$__combos = listener.register_many(this.hotkeys);
  },
  beforeDestroy() {
    listener.unregister_many(this.$__combos);
  },
};

export default Hotkeys;

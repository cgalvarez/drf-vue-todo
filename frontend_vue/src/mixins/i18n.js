import {camelCase, defaultsDeep, forIn} from 'lodash';


const I18n = {
  beforeCreate() {
    // TODO: Handle gender.
    // See https://github.com/kazupon/vue-i18n/blob/master/examples/formatting/custom/src/i18n.js

    //if (!this.constructor.extendOptions) return;
    // Every custom component is namespaced as `$vuetify.camelCasedComponentName`.
    const camelCasedName = camelCase(this.constructor.extendOptions.name);
    this.i18nNamespace = `$vuetify.${camelCasedName}.`;
  },

  created() {
    if (!this.i18nMessages) {
      // eslint-disable-next-line no-console
      return console.warn('You must preload localized messages into `i18nMessages` data property.');
    }

    const camelCasedName = this.i18nNamespace.split('.').slice(1).join('.');

    // `vue-i18n` has its own API to set the localized messages into a given
    // locale. We don't want to overwrite previous existing messages (like
    // Vuetify ones), but merge with new messages as new Vue components are
    // loaded (only on first load).
    forIn(this.i18nMessages, (componentMessages, locale) => {
      if (this.$i18n.messages[locale][camelCasedName] !== undefined) return;
      const merged = defaultsDeep({}, {$vuetify: componentMessages}, this.$i18n.messages[locale]);
      this.$i18n.setLocaleMessage(locale, merged);
    });

    if (this.localize) {
      this.localize();
      this.$watch('$i18n.locale', function(newLangCode) {
        // Calling `t()` or `_()` in the component template forces A LOT of
        // unnecessary updates (like when toggling collapsible list,
        // changing state of any form element, etc.).
        // That's why we centralize the localization of all messages in one
        // unique method. This approach is much more performant.
        this.localize();
        // Propagate locale change to Vuetify's own framework.
        this.$vuetify.lang.current = newLangCode;
        // NOTE: Sometimes the component does not get updated, so we force
        // for all newly localized messages it (in previous call).
        this.$nextTick(() => this.$forceUpdate());
      });
    }
  },

  methods: {
    /**
     * @summary Shortcut to localize messages of a specific component.
     * @param {string} id - The key path to fetch the localized message.
     * @param {Array} args - Any extra parameters passed to.
     * @returns {string} The localized message.
     * @see https://kazupon.github.io/vue-i18n/api/#t
     * @see https://github.com/vuetifyjs/vuetify/blob/master/packages/vuetify/src/components/Vuetify/mixins/lang.ts
     */
    _ (id, ...args) {
      return this.$vuetify.t(this.getTranslationPath(id), ...args);
    },

    /**
     * Shortcut to check if a localization for a specific component messages
     * exists (for current language).
     *
     * @param {string} id - The key path to fetch the localized message.
     * @param {Array} args - Any extra parameters passed to.
     * @returns {bool} Whether the localization exists or not.
     * @see https://kazupon.github.io/vue-i18n/api/#te
     * @see https://github.com/vuetifyjs/vuetify/blob/master/packages/vuetify/src/components/Vuetify/mixins/lang.ts
     */
    _te (id, ...args) {
      return this.$i18n.te(this.getTranslationPath(id), ...args);
    },

    /**
     * @summary Shortcut to localize with pluralization.
     * @param {string} id - The key path to fetch the localized message.
     * @param {Array} args - Any extra parameters passed to.
     * @returns {string} The localized message.
     * @see https://kazupon.github.io/vue-i18n/guide/pluralization.html
     * @see https://kazupon.github.io/vue-i18n/api/#tc
     */
    _tc (id, ...args) {
      return this.$i18n.tc(this.getTranslationPath(id), ...args);
    },

    getTranslationPath (id) {
      return this.i18nNamespace + id;
    },
  },
}

export default I18n;

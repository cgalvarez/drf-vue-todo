const Utils = {
  methods: {
    defer(callback, timeout=0) {
      let timer = setTimeout(() => {
        clearTimeout(timer);
        callback();
      }, timeout);
    },
  },
};

export default Utils;

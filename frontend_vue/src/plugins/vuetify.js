import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Vuetify from 'vuetify/lib';
import 'vuetify/src/stylus/app.styl';

// Import locales from Vuetify to properly localize its components.
import es from 'vuetify/es5/locale/es';
import en from 'vuetify/es5/locale/en';

import {mapValues} from 'lodash';


// Needs to be in use before being instantiated.
Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: navigator.language.split('-')[0] || 'en',
  fallbackLocale: 'en',
  silentFallbackWarn: true,
  messages: mapValues({es, en}, o => ({'$vuetify': o})),
});

Vue.use(Vuetify, {
  iconfont: 'mdi',
  lang: {
    // Define this method to avoid breaking i18n for Vuetify
    // core components, which rely on its own i18n framework.
    // See https://github.com/vuetifyjs/vuetify/blob/master/packages/vuetify/src/components/Vuetify/mixins/lang.ts
    t(key, ...params) {
      return i18n.t(key, params);
    },
  },
});

export {
  i18n,
};

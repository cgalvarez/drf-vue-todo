import * as types from './types';


const account = {
  namespaced: true,

  state: {
    timer: null,
    token: null,
  },

  mutations: {
    [types.ACCOUNT_SET_TOKEN] (state, payload) {
      state.token = payload.token;
    },
  },

  actions: {
    [types.ACCOUNT_SET_TOKEN]({ commit, dispatch, getters, state, rootGetters, rootState }, payload) {
      commit(types.ACCOUNT_SET_TOKEN, payload);
    },
    [types.ACCOUNT_CLEAR_TOKEN]({ commit }) {
      commit(types.ACCOUNT_SET_TOKEN, { token: undefined });
    },
  },

  getters: {
    isSignedIn(modState, modGetters, rootState) {
      return !!modState.token;
    },
    isSignedOut(modState, modGetters, rootState) {
      return !modState.token;
    },
    token(modState, modGetters, rootState) {
      return modState.token;
    },
  },
};

export default account;

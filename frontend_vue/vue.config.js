const BundleTracker = require('webpack-bundle-tracker');
const {join} = require('path');

const pages = {
  'public_frontend_vue_spa': {
    entry: './src/app/main.js',
    chunks: ['chunk-vendors'],
  },
};

// Prepare server stuff based on environment.
const port = 8080;
const devserverUrl = `http://localhost:${port}/`;
const isProduction = process.env.NODE_ENV === 'production';

// See https://cli.vuejs.org/config/#vue-config-js
module.exports = {
  pages,
  filenameHashing: false,
  productionSourceMap: false,
  // In production, fall back to Django’s standard static finder behavior.
  // In non-production, point to our own webpack development server.
  publicPath: isProduction ? '' : devserverUrl,
  outputDir: join('..', 'build', 'static', 'vue'),

  chainWebpack: config => {

    config.optimization
      .splitChunks({
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'chunk-vendors',
            chunks: 'all',
            priority: 1,
          },
        },
      });

    /**
     * As stated in VueJS docs, disable index generation for each page in
     * our multi-page VueJS app, because we are using Django as backend.
     * See https://cli.vuejs.org/guide/html-and-static-assets.html#prefetch
     */
    Object.keys(pages).forEach(page => {
      config.plugins.delete(`html-${page}`);
      config.plugins.delete(`preload-${page}`);
      config.plugins.delete(`prefetch-${page}`);
    });

    config
      .plugin('BundleTracker')
      .use(BundleTracker, [{
        filename: join('..', 'frontend_vue', 'webpack-stats.json'),
      }]);

    config.resolve.alias
      .set('__STATIC__', 'static');

    config.devServer
      .public(devserverUrl)
      .host('localhost')
      .port(port)
      .hotOnly(true)
      .watchOptions({poll: 1000})
      .headers({"Access-Control-Allow-Origin": ["*"]});

  },
};
